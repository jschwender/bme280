# BME280

Joachim Schwender, 2019

<p>
Reads data from a sensor hooked on I²C bus on a raspberry pi.
Data output can be adjusted to some degree to the needs.
Timestamp can be included in the output.</p>
<p>
The programming language C was choosen as it results in small,
fast and efficient program with small memory footprint. Depends
only on i2c lib, math lib and basic glibc stuff.
This makes it suitable for raspberry pi nano.</p>

The program required approximately 170 ms total time per cycle.
In continous mode this is approximately 8 measurements per second.
If the precision is released by reducing oversampling,
it may result in a much higher output freqency.

It can be launched by cron and output can be redirected to a file
that can be used for exampe for gnuplot or d3. Or output may be fed
into a monitoring software like zabbix.

<pre>command line options:
  -H   print data header
  -h <nnn>  (optional) height in m above NN, defaults to 0 m
  -d <device>   (optional) i2c device defaults to /dev/i2c-1
  -2    (optional) use alternative i2c-address 0x77 instead of default 0x76
  -f [seconds|iso|isomin|none]   timestamps in either seconds, ISO 8660 format (default), ISO8660 up to minutes or none
  -t [space|comma|semi]   output values with given separator (default is space)
  -u   output unit after each value (default is no unit)
  -c   loop endless (default one time only)
  -nocr   no cr at end of data line, adds a separator at EOL
  -v0     backwards mode (for bmp180): pressure and temperature
  -v1     output: time temperature dewpoint pressure pressureNN humidity abshumidity (DEFAULT)
  -v2     output: v1 values + airdensity, speed of sound
  -v3     output: v1 pressure,temperature
</pre>
Example output with header:<pre>
--x--
time temperature pressure humidity pressureNN
2019-10-07T18:02:00 19.72 980.33 43.44 1019.83
--x--
</pre>
Example output without timestamp and with header:<pre>
--x--
temperature pressure humidity pressureNN
19.71 980.59 43.74 1020.10
--x--

--x--
\# BME280 -H -v2
time temperature dewpoint pressure pressureNN humidity abshumidity airdensity soundspeed
2022-09-09T19:34:47 19.89 13.31 976.69 976.69 65.87 11.31 1.15422 344.44
--x--</pre>
## WARNING WARNING WARNING

The Bosch data sheet is unclear about use case and operating conditions. It explicitly specifies <b><em>home weather stations</em></b> as a use case, but there is no warning from condensating humidity conditions and dust ingress. <b>Both will damage the device</b>. In order tu use this device outdoor it has to be protected by a foil in order to keep dust and condensing humidity out.

For the BME280 the humidity range is specified up to 100%. But in a foodnote it clearly says only <b><em>non-condensing</em></b> climate. It is unclear how you can measure 100% humidity without condensing, this is maybe realistic under lab conditions but not outdoor. If you use these devices unprotected outside or in condensing climate like bath room or kitchen, it will deteriorate. The failure is not total: It will start permanently drifting and you will finally notice when temperature is too high or humidity hangs at 100% for longer period. This is not even specific to Bosch sensors, it applies to all mems sensors that are uncovered. Other vendors provide their sensors in a version with a seal foil attached on the hole. They are specified water proof. A reliable way of using this sensor is to combine it with a drift and humidity resistant sensor like DS18B20 nearby, and permanently watch the difference in temperature. As soon as the difference increases you know that it is time to replace the BMP/BME sensor.

Another issue is, that the sensor has <b>systematically too high temperature values</b>. This has nothing to to with the specified precision of the device, but with the fact that it always dissipates power, which heats up the chip. In real life these sensors are soldered on PBCs toghther with power regulator devices for 5V to 3.3V and pull up resisitors. All these parts also dissipate power. The maximum power that BME280 dissipates is "only" 2.1 mW. Sounds low? But there is no specification for a thermal resistance to ambient. In the user forum i found a value for the BME280: Junction to ambient: θ~JA =136 °C/W. This would result in +0,28°C offset. My measurements on a specific PCB show temperature offset of up to 2 °C above ambient in still air. So although this device has good temperature resolution, it would not be my recommendation to use it for temperature measurement. 

