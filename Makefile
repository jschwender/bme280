all:
	gcc -Wall -O3  BME280.c -o BME280 -lm

install:
	strip BME280 -o /usr/local/bin/BME280
	chown root.root /usr/local/bin/BME280
	chmod 755 /usr/local/bin/BME280
